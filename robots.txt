# https://kondratenko.ru

User-Agent: *
Disallow: /cgi-bin
Disallow: /webstat

Host: https://kondratenko.ru
Sitemap: https://kondratenko.ru/sitemap.xml